

//Inicialización
$(document).ready(function(){

      //Variables del DOM
      var ventana = $(window);
      var raqueta = $('#raqueta');
      var pelota = $('#pelota');
      var marcador = $('#marcador');

      //Variables generales
      var velocidad = parseInt(window.location.search.substr(1).split("=")[1]);
      var iniciado = false;
      var movimiento_pelota = { speed: velocidad,
                                x: 0,
                                y: 0,
                                directionX: -1,
                                directionY: -1 };

      //Constantes
      const margen_derecho = ventana.width()-5;
      const margen_izquierdo = 300;

      const tam_pelota = 20;
      const alt_raqueta = 30;
      const tam_raqueta = 200;

      //Estilos iniciales
      //RAQUETA
      raqueta.css("height", alt_raqueta)
             .css("width", tam_raqueta)
             .css("top", ventana.height()-60)
             .css("left", margen_izquierdo);
      pelota.css("height", tam_pelota)
            .css("width", tam_pelota)
            .css("left", raqueta.position().left+(tam_raqueta/2)-(tam_pelota/2))
            .css("top", raqueta.position().top-tam_pelota);

      //TABLERO
      $('#lineLeft, #lineRight').css("height", ventana.height()-10);
      $('#lineTop').css("width", ventana.width()-margen_izquierdo-5);
      $('#lineLeft').css("left", margen_izquierdo);
      $('#lineTop').css("left", margen_izquierdo);
      $('#lineRight').css("left", margen_derecho);

      //FUNCIONES
      function moverPelota(){
            calcularXY();

            pelota.css("left", movimiento_pelota.x).css("top", movimiento_pelota.y);
      };

      function calcularXY(){
            //Si la pelota está en el margen izquierdo se le asigna la dirección derecha
            //sino si está en el margen derecho se le asigna la dirección izquierda.
            if (pelota.position().left <= margen_izquierdo){

                  movimiento_pelota.directionX = 1;

            }else if ((pelota.position().left+tam_pelota) >= margen_derecho){

                  movimiento_pelota.directionX = -1;
            }

            //Si la pelota está en el margen superior se le asigna la dirección hacia abajo.
            //Sino si la pelota está en el margen inferior se le asigna la dirección hacia arriba.
            if (pelota.position().top <= 0){

                  movimiento_pelota.directionY = 1;

            }else if (  ((pelota.position().top+tam_pelota) >= raqueta.position().top)&&
                        (pelota.position().top < raqueta.position().top)){

                  if (  ((pelota.position().left+(tam_pelota/2)) >= raqueta.position().left)&&
                        ((pelota.position().left+(tam_pelota/2)) <= (raqueta.position().left+tam_raqueta))){

                        movimiento_pelota.directionY = -1;
                        marcador.text(parseInt(marcador.text())+1);
                  }

            }else if (pelota.position().top >= raqueta.position().top){

                  $('#empezar').text("Game Over").css("display", "inline");
                  movimiento_pelota.directionX = 0;
                  movimiento_pelota.directionY = 0;
            }

            if ((movimiento_pelota.directionX != 0)&&(movimiento_pelota.directionY != 0)){
                  //Si la dirección de la pelota es hacia la izquierda se desplaza restando a X.
                  //Sino si la dirección de la pelota es hacia la derecha se desplaza sumando a X.
                  if (movimiento_pelota.directionX == -1){
                        movimiento_pelota.x -= movimiento_pelota.speed;
                  }else{
                        movimiento_pelota.x += movimiento_pelota.speed;
                  }

                  //Si la dirección de la pelota es hacia arriba se desplaza restando a Y.
                  //sino si la dirección de la pelota es hacia abajo se desplaza sumando a Y.
                  if (movimiento_pelota.directionY == -1){
                        movimiento_pelota.y -= movimiento_pelota.speed;
                  }else{
                        movimiento_pelota.y += movimiento_pelota.speed;
                  }
            }
      }

      //Eventos
      ventana.click(function(){
            iniciado = true;
            setInterval(moverPelota, 1);
            $('#empezar').css("display", "none");
      });

      ventana.mousemove(function(evento){
            //Movimiento de la raqueta
            if (evento.clientX <= margen_izquierdo+(tam_raqueta/2)){
                  raqueta.css("left", margen_izquierdo);
            }else if (evento.clientX+100 >= margen_derecho){
                  raqueta.css("left", margen_derecho-tam_raqueta);
            }else{
                  raqueta.css("left", evento.clientX-(tam_raqueta/2));
            }

            //Movimiento de la pelota con la raqueta
            if (!iniciado){
                  pelota.css("left", raqueta.position().left+(tam_raqueta/2)-(tam_pelota/2));
                  movimiento_pelota.x = pelota.position().left;
                  movimiento_pelota.y = pelota.position().top;
            }
      });

});
